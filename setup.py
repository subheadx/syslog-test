from distutils.core import setup
import py2exe, socket, sys

setup(
    options = {'py2exe': {'bundle_files': 1, 'compressed': True}},
    console = ['syslog-connection-test.py'],
    zipfile = None,
)
