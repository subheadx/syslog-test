#!/bin/bash

SYSLOGSRV=10.15.31.43
CUR_DATE=`date +%Y%m%d%H%M`
echo "Sending test message to $SYSLOGSRV"
nc -w0 -u $SYSLOGSRV 514 <<< "Syslog test message $CUR_DATE"
